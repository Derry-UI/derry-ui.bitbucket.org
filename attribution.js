(function ($) {
	$.fn.attribute = function (options) {
		var settings = $.extend({
			text: "Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute Hello World, This is a test attribute",
			buttonCss: "outline: none; height:20px; border: none; font-size: 15px; color: rgb(0, 0, 0); border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(155, 205, 255);",
			paragraphCss: "padding: 5px; box-shadow: rgb(0, 0, 0) 0px 3px 6px -3px inset; margin: 0px; font-size: 15px; color: rgb(0, 0, 0); display: block; background: linear-gradient(rgb(64, 150, 238) 0%, rgb(96, 171, 248) 45%, rgb(155, 205, 255) 100%);"
		}, options);
		return this.each(function () {
			/*
					ButtonCss: 	   [0] = Outline
								   [1] = height
								   [2] = border
								   [3] = font-size
								   [4] = color
								   [5] = border-bottom-left-radius
								   [6] = border-bottom-right-radius
								   [7] = background-color

			    ParagraphCSS:      [1] = padding
			    				   [2] = box-shadow
			    				   [3] = margin
			    				   [4] = font-size
			    				   [5] = color
			    				   [6] = display
			    				   [7] = background

			*/
		//create variables
			var container;
				container = $(this);
			var attribute = "";
		//Grab all settings and put them into a variable for easy access
			var textSetting = settings.text;
			var buttonCssSetting = settings.buttonCss;
			var paragraphCSSSetting = settings.paragraphCss;
				container.css('position',"relative");
				container.css('z-index:', 9999);

				$(window).load(function(){
					attribute = "<div class='derry-attr'>"
					attribute += "<p class='derry-p-attr' style='"+paragraphCSSSetting+"'>"+textSetting+"</p>";
					attribute += "<button class='derry-button-attr' style='"+buttonCssSetting+"'>&copy;</button>";
					attribute += "</div>";
					container.after(attribute);
				
					container.next('.derry-attr').css("width", container.width());


					//apply beginning position
					var height = container.next('.derry-attr').height();
					var moveHeight = height - container.next('.derry-attr').find('button').height();
					container.next('.derry-attr').css("marginTop","-"+moveHeight+"px");
				


					//Animation
					container.next('.derry-attr').find('button').click(function(){
						if($(this).hasClass("isDown")){
							container.next('.derry-attr').find('p').animate({marginTop: "0px"});
							$(this).removeClass("isDown");
							
						}else{
							container.next('.derry-attr').find('p').animate({marginTop: moveHeight +"px"});
							$(this).css("position","relative");
							$(this).animate({bottom: "26px"});
							$(this).css("z-index","-1");
							$(this).addClass("isDown");
						}
					});
					container.next('.derry-attr').mouseleave(function(){
						container.next('.derry-attr').find('p').animate({marginTop: "0px"});
						$(this).find('button').animate({bottom: "-2px"});
						$(this).find('button').css("z-index","");
						$(this).find('button').removeClass("isDown");
					});
			});
		});
	};
}(jQuery));